package com.oleh.model;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.oleh.model.minesweeper.Minesweeper;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.Test;

public class MinesweeperTest {

    private static final Minesweeper minesweeper = mock(Minesweeper.class);

    @Test
    public void testGetBombMap1() {
        String[][] arr = {
                {"*", ".", ".", "*"},
                {"*", ".", ".", "*"},
                {"*", ".", ".", "*"},
                {"*", ".", ".", "*"},
        };
        when(minesweeper.getBombMap()).thenReturn(arr);
        assertArrayEquals(arr, minesweeper.getBombMap());
        verify(minesweeper, times(1)).getBombMap();
    }

    @Test
    public void testGetBombCountedMap() {
        String[][] arr = {
                {"*", "2", "2", "*"},
                {"*", "3", "3", "*"},
                {"*", "3", "3", "*"},
                {"*", "2", "2", "*"},
        };
        when(minesweeper.getBombCountedMap()).thenReturn(arr);
        assertArrayEquals(arr, minesweeper.getBombCountedMap());
        verify(minesweeper, times(1)).getBombCountedMap();
    }

    @Test
    public void testCutArray() {
        String[][] arr = {
                {"*", "2", "2", "*"},
                {"*", "3", "3", "*"},
                {"*", "3", "3", "*"},
                {"*", "2", "2", "*"},
        };
        String[][] arrRes = {
                {"3", "3"},
                {"3", "3"},
        };
        Minesweeper minesweeper = new Minesweeper(2, 2, 40);
        assertArrayEquals(arrRes, minesweeper.cutArray(arr));
    }

    @RepeatedTest(5)
    public void testGetBombMap2(RepetitionInfo repetitionInfo) {
        Minesweeper minesweeper = new Minesweeper(repetitionInfo.getCurrentRepetition(),
                repetitionInfo.getCurrentRepetition() + 2, repetitionInfo.getTotalRepetitions());
        String[][] array = new String[repetitionInfo.getCurrentRepetition()][repetitionInfo.getCurrentRepetition() + 2];

        assertEquals(array.length, minesweeper.getBombMap().length);
        assertEquals(array[0].length, minesweeper.getBombMap()[0].length);

    }

    @RepeatedTest(5)
    public void testGetBombCountedMap2(RepetitionInfo repetitionInfo) {
        Minesweeper minesweeper = new Minesweeper(repetitionInfo.getCurrentRepetition(),
                repetitionInfo.getCurrentRepetition() + 2, repetitionInfo.getTotalRepetitions());
        String[][] array = new String[repetitionInfo.getCurrentRepetition()][repetitionInfo.getCurrentRepetition() + 2];

        assertEquals(array.length, minesweeper.getBombCountedMap().length);
        assertEquals(array[0].length, minesweeper.getBombCountedMap()[0].length);
    }

    @Test
    public void testGetBombMap3() {
        Minesweeper minesweeper = new Minesweeper(4, 5, 34);
        assertNotNull(minesweeper.getBombMap());
    }

    @Test
    public void testGetBombCountedMap3() {
        Minesweeper minesweeper = new Minesweeper(4, 5, 34);
        assertNotNull(minesweeper.getBombCountedMap());
    }

}
