package com.oleh.model;

import static org.junit.jupiter.api.Assertions.*;

import com.oleh.model.longestPlateau.LongestPlateau;
import org.junit.Test;

public class LongestPlateauTest {
    int[] a = {4, 2, 5, 3, 4, 4, 4, 2, 1};
    private final LongestPlateau longestPlateau = new LongestPlateau(a);

    @Test
    public void testGetLength() {
        assertEquals(3, longestPlateau.getLength());
    }

    @Test
    public void testGetLocation() {
        assertEquals(4, longestPlateau.getLocation());
    }

    @Test
    public void testCalculate() {
        assertEquals(4, longestPlateau.getLocation());
        assertEquals(3, longestPlateau.getLength());

    }
}
