package com.oleh;

import com.oleh.model.LongestPlateauTest;
import com.oleh.model.MinesweeperTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({LongestPlateauTest.class, MinesweeperTest.class})
public class AllTest {

}
