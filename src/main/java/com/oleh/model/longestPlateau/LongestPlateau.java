package com.oleh.model.longestPlateau;

public class LongestPlateau {
    private int[] arr;
    private int length;
    private int location;

    public LongestPlateau(int[] arr) {
        this.arr = arr;
        calculate();
    }


    /**
     * In this method longest plato is calculating
     */
    private void calculate() {
        int location = 0;
        int longestLocation = 0;
        int longestLength = 0;
        int len = 0;
        int value = arr[0];

        for (int i = 1; i < arr.length; i++) {

            boolean firstRule = false;
            boolean secondRule = false;

            if (arr[i] == value && arr[i - 1] == value) {
                firstRule = true;
                len = i - location + 1;
            } else if (i != arr.length - 1 && arr[i - 1] < arr[i]) {
                value = arr[i];
                location = i;
            }

            if (i == arr.length - 1 || arr[i + 1] < value) {
                secondRule = true;
            }

            if (firstRule && secondRule) {
                if (len > longestLength) {
                    longestLength = len;
                    longestLocation = location;
                }
            }
        }

        this.length = longestLength;
        this.location = longestLocation;

    }

    public int getLength() {
        return length;
    }

    public int getLocation() {
        return location;
    }

}
