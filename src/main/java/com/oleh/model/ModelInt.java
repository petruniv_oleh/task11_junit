package com.oleh.model;

import com.oleh.model.longestPlateau.LongestPlateau;
import com.oleh.model.minesweeper.Minesweeper;

public interface ModelInt {
    LongestPlateau makeLongestPlateau(int[] arr);
    Minesweeper makeMinesweeper(int M, int N, int p);
    String[][] makeBombMap(Minesweeper minesweeper);
    String[][] makeBombCountedMap(Minesweeper minesweeper);
}
