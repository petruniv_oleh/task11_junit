package com.oleh.model;

import com.oleh.model.longestPlateau.LongestPlateau;
import com.oleh.model.minesweeper.Minesweeper;

public class Model implements ModelInt {


    @Override
    public LongestPlateau makeLongestPlateau(int[] arr) {
        return new LongestPlateau(arr);
    }

    @Override
    public Minesweeper makeMinesweeper(int M, int N, int p) {
        return new Minesweeper(M, N, p);
    }

    @Override
    public String[][] makeBombMap(Minesweeper minesweeper) {
        return minesweeper.getBombMap();
    }

    @Override
    public String[][] makeBombCountedMap(Minesweeper minesweeper) {
        return minesweeper.getBombCountedMap();
    }
}
