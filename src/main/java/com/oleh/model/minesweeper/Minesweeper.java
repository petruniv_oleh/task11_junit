package com.oleh.model.minesweeper;

import java.util.Random;

public class Minesweeper {
    private boolean[][] gameArea;
    private int possability;


    public Minesweeper(int M, int N, int possability) {
        gameArea = new boolean[M + 2][N + 2];
        this.possability = possability;
        fillInArray();
    }

    private void fillInArray() {
        Random random = new Random();
        for (int i = 1; i < gameArea.length - 1; i++) {
            for (int j = 1; j < gameArea[0].length - 1; j++) {
                if (random.nextInt(100) < possability) {
                    gameArea[i][j] = true;
                }
            }
        }
    }

    /**
     * Here map of bombs is creating
     *
     * @return map of bombs
     */
    public String[][] makeMinesMap() {
        String[][] minesMap = new String[gameArea.length][gameArea[0].length];
        for (int i = 0; i < minesMap.length; i++) {
            for (int j = 0; j < minesMap[0].length; j++) {
                if (gameArea[i][j]) {
                    minesMap[i][j] = "*";
                } else {
                    minesMap[i][j] = ".";
                }
            }
        }
        return minesMap;
    }

    /**
     * Here map of the game with counted bombs near safe area is creating
     *
     * @return map with counted bombs
     */
    public String[][] makeBombCountedMap() {
        String[][] bombCountedMap = makeMinesMap();

        for (int i = 1; i < bombCountedMap.length - 1; i++) {
            for (int j = 1; j < bombCountedMap[0].length - 1; j++) {
                if (bombCountedMap[i][j].equals(".")) {
                    int counter = 0;
                    if (bombCountedMap[i - 1][j].equals("*")) {
                        counter++;
                    }
                    if (bombCountedMap[i - 1][j - 1].equals("*")) {
                        counter++;
                    }
                    if (bombCountedMap[i][j - 1].equals("*")) {
                        counter++;
                    }
                    if (bombCountedMap[i + 1][j].equals("*")) {
                        counter++;
                    }
                    if (bombCountedMap[i + 1][j + 1].equals("*")) {
                        counter++;
                    }
                    if (bombCountedMap[i][j + 1].equals("*")) {
                        counter++;
                    }
                    if (bombCountedMap[i - 1][j + 1].equals("*")) {
                        counter++;
                    }
                    if (bombCountedMap[i + 1][j - 1].equals("*")) {
                        counter++;
                    }
                    bombCountedMap[i][j] = ""+counter;
                }
            }
        }

        return bombCountedMap;
    }


    /**
     * Here all the useless borders of arrays are deleted
     *
     * @param arrToCut
     * @return array without additional borders
     */
    public String[][] cutArray(String [][] arrToCut) {
        String [][] resArr = new String[arrToCut.length - 2][arrToCut[0].length - 2];
        for (int i = 1, k = 0; i < arrToCut.length - 1; i++, k++) {
            for (int j = 1, l = 0; j < arrToCut[0].length - 1; j++, l++) {
                resArr[k][l] = arrToCut[i][j];
            }
        }
        return resArr;
    }

    public String[][] getBombMap(){
        return cutArray(makeMinesMap());
    }

    public String[][] getBombCountedMap(){
        return cutArray(makeBombCountedMap());
    }

}
