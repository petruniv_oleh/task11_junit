package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.longestPlateau.LongestPlateau;
import com.oleh.model.minesweeper.Minesweeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View implements ViewInt {

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger(View.class.getName());
    Scanner scanner = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Longest plateau");
        menuMap.put("2", "2. Minesweeper");

        menuMapMethods.put("1", this::longestPlateau);
        menuMapMethods.put("2", this::minesweeper);



    }
    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }

    @Override
    public void minesweeper() {
        logger.info("Invoke minesweeper");
        System.out.println("Enter M: ");
        int m = scanner.nextInt();
        System.out.println("Enter N: ");
        int n = scanner.nextInt();
        System.out.println("Enter p: ");
        int p = scanner.nextInt();

        logger.debug("Entered M("+m+"), N("+n+"), p("+p+")");
        Minesweeper minesweeper = controller.getMinesweeper(m, n, p);
        System.out.println("Bomb map (* - bomb, . - safe): ");
        printCharArray(controller.getBombMap(minesweeper));
        System.out.println("Bomb counted map: ");
        printCharArray(controller.getBombCountedMap(minesweeper));

    }

    private void printCharArray(String[][] arr){
        logger.info("printing array");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print("\t"+arr[i][j]);
            }
            System.out.println();
        }
    }

    @Override
    public void longestPlateau() {
        logger.info("Invoke longestPlateau");
        int[] arr = {1,2,4,4,3,5,5,5,3,3,3,8,8,8,8};
        LongestPlateau longestPlateau = controller.getLongestPlateau(arr);
        System.out.println("Longest plateau is located: "+longestPlateau.getLocation());
        System.out.println("Longest plateau length: "+longestPlateau.getLength());
    }
}
