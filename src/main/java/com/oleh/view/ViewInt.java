package com.oleh.view;

public interface ViewInt {
    void showMenu();
    void minesweeper();
    void longestPlateau();
}
