package com.oleh.view;

@FunctionalInterface
public interface Printable {
    void print() throws IllegalAccessException;
}
