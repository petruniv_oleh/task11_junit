package com.oleh.controller;

import com.oleh.model.longestPlateau.LongestPlateau;
import com.oleh.model.minesweeper.Minesweeper;

public interface ControllerInt {
    LongestPlateau getLongestPlateau(int[] arr);
    Minesweeper getMinesweeper(int M, int N, int p);
    String[][] getBombMap(Minesweeper minesweeper);
    String[][] getBombCountedMap(Minesweeper minesweeper);
}
