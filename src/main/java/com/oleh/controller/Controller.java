package com.oleh.controller;

import com.oleh.model.Model;
import com.oleh.model.longestPlateau.LongestPlateau;
import com.oleh.model.minesweeper.Minesweeper;

public class Controller implements ControllerInt{

    Model model;

    public Controller() {
        model = new Model();
    }

    @Override
    public LongestPlateau getLongestPlateau(int[] arr) {
        return model.makeLongestPlateau(arr);
    }

    @Override
    public Minesweeper getMinesweeper(int M, int N, int p) {
        return model.makeMinesweeper(M, N, p);
    }

    @Override
    public String[][] getBombMap(Minesweeper minesweeper) {
        return model.makeBombMap(minesweeper);
    }

    @Override
    public String[][] getBombCountedMap(Minesweeper minesweeper) {
        return model.makeBombCountedMap(minesweeper);
    }
}
